<?php
/**
 * @package    authdrudle
 * @copyright  2011 suton.tamimy@yahoo.com
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 * Authentication Plugin: Drudle Single Sign-on
 *
 * This module will look for a Drupal cookie that represents a valid,
 * authenticated session, and will use it to create an authenticated Moodle
 * session for the same user. The Drupal user will be synchronized with the
 * corresponding user in Moodle. If the user does not yet exist in Moodle, it
 * will be created.
 */

// This must be accessed from a Moodle page only!
if (!defined('MOODLE_INTERNAL')) {
   die('Direct access to this script is forbidden.');
}

require_once($CFG->libdir.'/authlib.php');
require_once($CFG->libdir.'/adodb/adodb.inc.php');

// Drudle SSO authentication plugin.
class auth_plugin_drudle extends auth_plugin_base {

   // Constructor
   function auth_plugin_drudle() {
      $this->authtype = 'drudle';
      $this->config = get_config('auth/drudle');
   }
   
   // Function is called to validate data before it's inserted in config_plugin
   function validate_form(&$form, &$err) {
      if (empty($form->location) || (substr($form->location,0,1) != '/')) {
         $form->location = '/';
      }
      if (!empty($form->fidfirst) && !ctype_digit($form->fidfirst)) {
         $form->fidfirst = '';
      }
      if (!empty($form->fidlast) && !ctype_digit($form->fidlast)) {
         $form->fidlast = '';
      }
   }

   // Function to generate custom query for Drudle that will return:
   // uid, username, firstname, lastname, email
   // if $sid is provided, it will create a query to return the associated user
   function sql_drudle_users($sid = '') {
      $sql = "SELECT ".
         "t1.uid AS 'uid', ".
         "t1.name AS 'username', ".
         "COALESCE(t1.language,'en') AS 'language', ";

      if (empty($this->config->fidfirst)) {
            $sql .= "'SSO' AS 'firstname', ";
      } else {
            $sql .= "COALESCE(t2.value, 'SSO') AS 'firstname', ";
      }

      if (empty($this->config->fidlast)) {
            $sql .= "t1.name AS 'lastname', ";
      } else {
            $sql .= "COALESCE(t3.value, t1.name) AS 'lastname', ";
      }

      $sql .= "t1.mail AS 'email' FROM ".
         "{$this->config->name}.{$this->config->tblprefix}users t1 ";
      
      if (!empty($this->config->fidfirst)) {
         $sql .= "LEFT JOIN {$this->config->name}.{$this->config->tblprefix}".
            "profile_values t2 ON t1.uid = t2.uid ".
            "AND t2.fid = {$this->config->fidfirst} ";
      }
      
      if (!empty($this->config->fidlast)) {
         $sql .= "LEFT JOIN {$this->config->name}.{$this->config->tblprefix}".
            "profile_values t3 ON t1.uid = t3.uid ".
            "AND t3.fid = {$this->config->fidlast} ";
      }
      
      $sql .= "WHERE t1.uid > 0";

      if (!empty($sid)) {
         $sql .= " AND t1.uid IN ".
            "(SELECT uid FROM {$this->config->name}.{$this->config->tblprefix}".
            "sessions WHERE sid = '$sid')";
      }
                  
      return $sql.';';
   }
   
   // This plugin is for SSO only; Drupal handles the login
   function user_login($username, $password) { return false; }

   // Function to enable SSO (it runs before user_login() is called)
   // If a valid Drudle session is not found, the user will be forced to the
   // login page where some other plugin will have to authenticate the user
	 function loginpage_hook_new() {
		 global $CFG, $USER, $SESSION, $DB;
		 
		 // Check if we have a Drupal session.
     // $cookie = 'SESS'.md5($_SERVER['HTTP_HOST'].rtrim($this->config->location,'/'));
     // $drudle_sid = $_COOKIE[$cookie];
			
		//	print_r($drudle_sid);
			
			$USER = get_complete_user_data('id', 2);
      complete_user_login($USER);
			
			// no wantsurl stored or external link. Go to homepage.
			$urltogo = $CFG->wwwroot.'/';
			unset($SESSION->wantsurl);
      redirect($urltogo);
	 }
	 
   function loginpage_hook_old() {
      global $CFG, $USER, $SESSION, $DB;
      
      // Check if we have a Drupal session.
      $cookie = 'SESS'.md5($_SERVER['HTTP_HOST'].rtrim($this->config->location,'/'));
      $drudle_sid = $_COOKIE[$cookie];
      if (empty($drudle_sid)) {
         return; // Drudle session does not exist; send user to login page
      }

      // Verify the authenticity of the Drupal session ID
      $drudle_user = $DB->get_record_sql($this->sql_drudle_users($drudle_sid));
      if ($drudle_user === false) {
            // the session ID is not valid
            if (isloggedin() && !isguestuser()) {
               // the user is logged-off of Drudle but still logged-in on Moodle
               // so we must now log-off the user from Moodle...
               require_logout();
            }
            return;
      }
      
      // The Drupal session is valid; now check if Moodle is logged in...
      if (isloggedin() && !isguestuser()) { return; }

      // Moodle is not logged in so fetch or create the corresponding user
      $user = $this->create_update_user($drudle_user);
      if (empty($user)) {
         // Something went wrong while creating the user
         print_error('auth_drudlecreateaccount', 'auth_drudle',
            $drudle_user->username);
         unset($drudle_user);
         return;
      }
      unset($drudle_user);

      // complete login
      $USER = get_complete_user_data('id', $user->id);
      complete_user_login($USER);

      // redirect
      if (isset($SESSION->wantsurl) and
         (strpos($SESSION->wantsurl, $CFG->wwwroot) == 0)) {
            // the URL is set and within Moodle's environment
            $urltogo = $SESSION->wantsurl;
            unset($SESSION->wantsurl);
      } else {
            // no wantsurl stored or external link. Go to homepage.
            $urltogo = $CFG->wwwroot.'/';
            unset($SESSION->wantsurl);
      }
      redirect($urltogo);
   }
   
   // function to grab Moodle user and update their fields then return the
   // account. If the account does not exist, create it.
   // Returns: the Moodle user (array) associated with drudle user argument
   function create_update_user($drudle_user) {
      global $CFG, $DB;

      $username = $drudle_user->username;
      $user = $DB->get_record('user',
         array('username'=>$username, 'mnethostid'=>$CFG->mnet_localhost_id));
      if (empty($user)) {
            // build the new user object to be put into the Moodle database
            $user = new object();
            $user->username   = $username;
            $user->firstname  = $drudle_user->firstname;
            $user->lastname   = $drudle_user->lastname;
            $user->auth       = $this->authtype;
            $user->mnethostid = $CFG->mnet_localhost_id;
            $user->lang       = str_replace('-', '_', $drudle_user->language);
            $user->confirmed  = 1;
            $user->email      = $drudle_user->email;
            $user->idnumber   = $drudle_user->uid;
            $user->modified	= time();

            // add the new Drudle user to Moodle
            $uid = $DB->insert_record('user',$user);
            $user = $DB->get_record('user',
               array('username'=>$username, 'mnethostid'=>$CFG->mnet_localhost_id));
            if (!$user) {
               print_error('auth_drudlecantinsert','auth_db',$username);
            }
      } else {
            // Update user information
            if (strcmp(
               $user->email.$user->firstname.$user->lastname.$user->lang,
               $drudle_user->email.$drudle_user->firstname.
               $drudle_user->lastname.str_replace('-','_',$drudle_user->language))
               != 0) {
                  $user->email = $drudle_user->email;
                  $user->firstname = $drudle_user->firstname;
                  $user->lastname = $drudle_user->lastname;
                  $user->lang = str_replace('-', '_', $drudle_user->language);
                  if (!$DB->update_record('user', $user)) {
                        print_error('auth_drudlecantupdate','auth_db',$username);
                  }
            }
      }
      return $user;
   }
   
   // function that is called upon the user logging out
   function logoutpage_hook_old() {
      global $CFG, $DB;
      
      // Check whether we still have a Drudle session.
      $cookie = 'SESS'.md5($_SERVER['HTTP_HOST'].rtrim($this->config->location,'/'));
      $drudle_sid = $_COOKIE[$cookie];
      if (empty($drudle_sid)) {
         return; // the Drudle session has already been terminated
      }
      
      // remove the session ID from Drudle Database
      $result = $DB->execute(
         "DELETE FROM {$this->config->name}.{$this->config->tblprefix}sessions ".
         "WHERE sid = '$drudle_sid'");      
      return $result;
   }
   
   // cron synchronization script
   // $do_updates: true to update existing accounts (and add new Drudle accounts)
   function sync_users($do_updates=false) {
      global $CFG, $DB;
      
      // process users in Moodle that no longer exist in Drudle
      if (!empty($this->config->removeuser)) {
         // find obsolete users
         $remove_users = $DB->get_records_sql(
            "SELECT id, username, email, auth ".
            "FROM {$CFG->prefix}user ".
            "WHERE auth='{$this->authtype}' ".
            "AND deleted=0 ".
            "AND username NOT IN ".
            "(SELECT name FROM ".
            "{$this->config->name}.{$this->config->tblprefix}users)");
         if (!empty($remove_users)) {
            print_string('auth_drudleuserstoremove','auth_drudle',
               count($remove_users));
            echo "\n";

            foreach ($remove_users as $user) {
               if ($this->config->removeuser == AUTH_REMOVEUSER_FULLDELETE) {
                     if (delete_user($user)) {
                           echo "\t";
                           print_string('auth_drudledeleteuser', 'auth_db',
                              array('name'=>$user->username, 'id'=>$user->id));
                           echo "\n";
                     } else {
                           echo "\t";
                           print_string('auth_drudledeleteusererror', 'auth_db',
                              $user->username);
                           echo "\n";
                     }
               } else if ($this->config->removeuser == AUTH_REMOVEUSER_SUSPEND) {
                     $updateuser = new object();
                     $updateuser->id    = $user->id;
                     $updateuser->auth = 'nologin';
                     if ($DB->update_record('user', $updateuser)) {
                           echo "\t";
                           print_string('auth_drudlesuspenduser', 'auth_db',
                              array('name'=>$user->username, 'id'=>$user->id));
                           echo "\n";
                     } else {
                           echo "\t";
                           print_string('auth_drudlesuspendusererror', 'auth_db',
                              $user->username);
                           echo "\n";
                     }
               }
            }
         }
         unset($remove_users); // free mem!
      }
      
      // sync users in Drudle with users in Moodle (adding users if needed)
      if ($do_updates) {
         // Pull all the Drudle users with their information
         $users_to_sync = $DB->get_records_sql($this->sql_drudle_users());
         if (empty($users_to_sync)) {
               print_string('auth_drudlenorecords','auth_drudle');
               return false;
         } else {
            // sync users in Drudle with users in Moodle (adding users if needed)
            print_string('auth_drudleuserstoupdate','auth_drudle',
               count($users_to_sync));
            echo "\n";
            foreach ($users_to_sync as $user) {
               print_string('auth_drudleupdateuser', 'auth_drudle',
                  $user->username);
               echo "\n";
               $this->create_update_user($user);
            }
         }
         unset($users_to_sync); // free mem!
      }
   }

   // Function called by admin/auth.php to print a form for configuring plugin
   // @param array $page An object containing all the data for this page.
   function config_form($config, $err, $user_fields) {
      include 'config.html';
   }

   // Processes and stores configuration data for this authentication plugin.
   function process_config($config) {
      // set to defaults if undefined
      if (!isset($config->location)) {
            $config->location = '/';
      }
      if (!isset($config->name)) {
            $config->name = '';
      }
      if (!isset($config->tblprefix)) {
            $config->tblprefix = 'drp_';
      }
      if (!isset($config->fidfirst)) {
            $config->fidfirst = '1';
      }
      if (!isset($config->fidlast)) {
            $config->fidlast = '2';
      }
      if (empty($config->debugauthdrudle)) {
            $config->debugauthdrudle = 0;
      }
      if (!isset($config->removeuser)) {
            $config->removeuser = AUTH_REMOVEUSER_KEEP;
      }

      // save settings
      set_config('location',        $config->location,        'auth/drudle');
      set_config('name',            $config->name,            'auth/drudle');
      set_config('tblprefix',       $config->tblprefix,       'auth/drudle');
      set_config('fidfirst',        $config->fidfirst,        'auth/drudle');
      set_config('fidlast',         $config->fidlast,         'auth/drudle');
      set_config('debugauthdrudle', $config->debugauthdrudle, 'auth/drudle');
      set_config('removeuser',      $config->removeuser,      'auth/drudle');

      return true;
   }
}