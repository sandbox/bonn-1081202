<?php

/**
 * Web service local plugin Drudle external functions and service definitions.
 *
 * @package    localdrudle
 * @copyright  2011 suton.tamimy@yahoo.com
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// We defined the web service functions to install.
$functions = array(
	'local_drudle_hello_world' => array(
		'classname'   => 'local_drudle_external',
		'methodname'  => 'hello_world',
		'classpath'   => 'local/drudle/externallib.php',
		'description' => 'Return Hello World FIRSTNAME. Can change the text (Hello World) sending a new text as parameter',
		'type'        => 'read'
	),
	'local_drudle_login' => array(
		'classname'   => 'local_drudle_external',
		'methodname'  => 'login',
		'classpath'   => 'local/drudle/externallib.php',
		'description' => 'Login via service',
		'type'        => 'read'
	)
);

// We define the services to install as pre-build services. A pre-build service is not editable by administrator.
$services = array(
	'Drudle' => array(
		'functions' => array (
			'local_drudle_hello_world',
			'local_drudle_login',
			'moodle_course_create_courses',
			'moodle_course_get_courses',
			'moodle_enrol_get_enrolled_users',
			'moodle_enrol_manual_enrol_users',
			'moodle_file_get_files',
			'moodle_file_upload',
			'moodle_group_add_groupmembers',
			'moodle_group_create_groups',
			'moodle_group_delete_groupmembers',
			'moodle_group_delete_groups',
			'moodle_group_get_course_groups',
			'moodle_group_get_groupmembers',
			'moodle_group_get_groups',
			'moodle_role_assign',
			'moodle_role_unassign',
			'moodle_user_create_users',
			'moodle_user_delete_users',
			'moodle_user_get_users_by_id',
			'moodle_user_update_users'
		),
		'restrictedusers' => 1,
		'enabled'=>1,
		'shortname' => 'drudle'
	)
);