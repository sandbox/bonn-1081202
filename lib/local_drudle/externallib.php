<?php

/**
 * External Web Service Drudle
 *
 * @package    localdrudle
 * @copyright  2011 suton.tamimy@yahoo.com
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once($CFG->libdir . "/externallib.php");

class local_drudle_external extends external_api {
	
	/*** METHOD hello_world ***/
	/**
	* Returns description of method parameters
	* @return external_function_parameters
	*/
	public static function hello_world_parameters() {
		return new external_function_parameters(
						array('welcomemessage' => new external_value(PARAM_TEXT, 'The welcome message. By default it is "Hello world,"', VALUE_DEFAULT, 'Hello world, '))
		);
	}

	/**
	* Returns welcome message
	* @return string welcome message
	*/
	public static function hello_world($welcomemessage = 'Hello world, ') {
		global $USER;

		//Parameter validation
		//REQUIRED
		$params = self::validate_parameters(self::hello_world_parameters(),
						array('welcomemessage' => $welcomemessage));

		//Context validation
		//OPTIONAL but in most web service it should present
		$context = get_context_instance(CONTEXT_USER, $USER->id);
		self::validate_context($context);

		//Capability checking
		//OPTIONAL but in most web service it should present
		if (!has_capability('moodle/user:viewdetails', $context)) {
				throw new moodle_exception('cannotviewprofile');
		}

		return $params['welcomemessage'] . $USER->firstname ;;
	}

	/**
	* Returns description of method result value
	* @return external_description
	*/
	public static function hello_world_returns() {
		return new external_value(PARAM_TEXT, 'The welcome message + user first name');
	}
	
	/*** METHOD login ***/
	/**
	* Returns description of method parameters
	* @return external_function_parameters
	*/
	public static function login_parameters() {
		return new external_function_parameters(
						array('welcomemessage' => new external_value(PARAM_TEXT, 'The welcome message. By default it is "Hello world,"', VALUE_DEFAULT, 'Hello world, '))
		);
	}

	/**
	* Returns welcome message
	* @return string welcome message
	*/
	public static function login($welcomemessage = 'Hello world, ') {
		global $USER;

		//Parameter validation
		//REQUIRED
		$params = self::validate_parameters(self::hello_world_parameters(),
						array('welcomemessage' => $welcomemessage));

		//Context validation
		//OPTIONAL but in most web service it should present
		$context = get_context_instance(CONTEXT_USER, $USER->id);
		self::validate_context($context);

		//Capability checking
		//OPTIONAL but in most web service it should present
		if (!has_capability('moodle/user:viewdetails', $context)) {
				throw new moodle_exception('cannotviewprofile');
		}

		return $params['welcomemessage'] . $USER->firstname ;;
	}

	/**
	* Returns description of method result value
	* @return external_description
	*/
	public static function login_returns() {
		return new external_value(PARAM_TEXT, 'The welcome message + user first name');
	}
	
}