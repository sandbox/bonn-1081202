<?php

/**
 * Drudle Web service plugin
 * @package   localdrudle
 * @copyright 2011 suton.tamimy@yahoo.com
 * @author    Suton Tamimy
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Drudle Web service';