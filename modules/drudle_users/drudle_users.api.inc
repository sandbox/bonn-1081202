<?php

function drudle_users_api_get_role_id($name) {
	$roles_table = user_roles(TRUE); // loads the roles table into an array
	return array_search($name, $roles_table); // where $rid == the role id of the $name variable name.
}

function drudle_users_api_create_role($name) {
	$rid = drudle_users_api_get_role_id($name);
	
	if(!$rid) {
		// create role
		require_once(drupal_get_path('module', 'user') . "/user.admin.inc");

		$form_id = "user_admin_new_role";
		$form_values = array();
		$form_values["name"] = $name;
		$form_values["op"] = t('Add role');
		$form_state = array();
		$form_state["values"] = $form_values;
		
		/* The form is generated based on the the URL. or $_GET['q'] value. see user_admin_role()
		 * hope in the next release the developers can give is either a function 
		 * to directly create roles or not generate the form based on the url.
		 */
		$q = $_GET['q'];
		$_GET['q'] = "";
		drupal_execute($form_id, $form_state);
		$_GET['q'] = $q;
		
		form_get_errors();

		$rid = drudle_users_api_get_role_id($name);
	}
	return $rid;
}

// credits: http://drupal.org/node/730554#comment-5168642
function drudle_users_api_profile_field_add($properties) {
	// check if same field exists
	$fid = (int)db_fetch_array(db_query("SELECT fid FROM {profile_fields} WHERE name='%s'", $properties['name']));
  
	if(!$fid) {
		drupal_write_record('profile_fields', $properties);

		//this ain't such a great workaround
		$category = ( array_key_exists('category',$properties) ? $properties['category'] : t('(none)') );

		drupal_set_message(t('The field %field has been created under category %category.', array('%field' => $properties['title'], '%category' => $category)));
		watchdog('profile', 'Profile field %field added under category %category.', array('%field' => $properties['title'], '%category' => $category), WATCHDOG_NOTICE, l(t('view'), 'admin/user/profile'));
	}
}

// credits: http://drupal.org/node/730554#comment-5168642
function drudle_users_profile_field_remove($name, $remove_values=TRUE) {
  //could be bad ramifications from deleting a profile field but *not* deleting its values (fid seems not to be reused) ?
  if($remove_values) {
    // @todo not sure how db-independent this SQL is ...
    db_query("DELETE FROM {profile_values} WHERE fid IN (SELECT fid FROM {profile_fields} WHERE name='%s')",$name);
  }
  db_query("DELETE FROM {profile_fields} WHERE name='%s'", $name);
  watchdog('profile', 'Profile field %name was deleted.', array('%name' => $name), WATCHDOG_NOTICE, l(t('view'), 'admin/user/profile')); #TODO: also mention whether values were killed
}