<?php

define('DOMAIN_NAME', variable_get('drudle_domain_name', ''));
define('SECURITY_KEY', variable_get('drudle_security_key', ''));

// create single moodle user
// return 1 -> moodle user id
function drudle_users_rest_create_user($account) {
	module_load_include('inc', 'drudle', 'drudle.api');
	
	$t = get_t();
	$new_user = new stdClass();
	$new_user->username = $account->name;
	$new_user->password = $account->pass_clean;
	$new_user->firstname = $account->name;
	$new_user->lastname = 'Lastname';
	$new_user->email = $account->mail;
	$new_user->auth = 'manual';
	//$new_user->idnumber = 'testidnumber1';
	$new_user->lang = 'en';
	$new_user->theme = 'standard';
	//$new_user->timezone = '-12.5';
	$new_user->mailformat = 0;
	$new_user->description = 'Hello World!';
	//$new_user->city = 'testcity1';
	//$new_user->country = 'au';
	
	$method = 'moodle_user_create_users';
	$arguments = array('users' => array($new_user));
	
	$result = drudle_api_parsexml(drudle_api_callrest($method, $arguments, DOMAIN_NAME, SECURITY_KEY));
	drupal_set_message($t('A Moodle user @username with an id @id has been created.', array(
		'@username' => $result[0]['username'],
		'@id'		=> $result[0]['id']
	)));
	
	return empty($result[0]) ? 
		drupal_set_message(t('Failed adding Moodle user. Check security key and make sure it works.'), 'error') : 
		$result[0];
}

function drudle_users_rest_delete_user($uid) {
	$t = get_t();
	module_load_include('inc', 'drudle', 'drudle.api');
	
	$method = 'moodle_user_delete_users';
	$arguments = array('userids' => array($uid));
	
	// the method return no response, so we return nothing
	drudle_api_callrest($method, $arguments, DOMAIN_NAME, SECURITY_KEY);
	drupal_set_message($t('A Moodle user with an id @id has been deleted.', array(
		'@id'		=> $uid
	)));
}

//@todo _create_coursecreator, create_manager -> also assign security key for drupal user