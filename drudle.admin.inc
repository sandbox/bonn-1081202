<?php

function drudle_admin_settings() {
	$form['drudle_admin_settings'] = array('#type' => 'fieldset', '#title' => t('Base Configuration'));
	$form['drudle_admin_settings']['drudle_domain_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Moodle installation path'),
    '#default_value' => variable_get('drudle_domain_name', ''),
    '#description' => t('Your Moodle URL. For security reason, you are encouraged to use HTTPS protocol. <strong>Do not put ending slash</strong>.'),
    '#required' => TRUE
  );
	
	$form['drudle_admin_settings']['drudle_security_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Moodle Global Security Key/Token'),
    '#default_value' => variable_get('drudle_security_key', ''),
    '#description' => t('The unique key to communicate with Moodle Web Service. 
			You should have the token if you follow INSTALL.txt. Do not share this key.'),
    '#required' => TRUE
  );
	
	return system_settings_form($form);
}

function drudle_rest_test() {
	global $user;
	
	$result				= array();
	$domain_name	= variable_get('drudle_domain_name', '');
	$security_key	= variable_get('drudle_security_key', '');
	
	if($domain_name && $security_key) {
		module_load_include('inc', 'drudle', 'drudle.api');
	
		$method['uids']['method'] = 'moodle_user_get_users_by_id';
		$method['uids']['arguments'] = array('userids' => array(2,3));
		$result['uids'] = drudle_api_parsexml(drudle_api_callrest($method['uids']['method'], $method['uids']['arguments'], $domain_name, $security_key));

		$method['courses']['method'] = 'moodle_course_get_courses';
		$method['courses']['arguments'] = array('options' => array('ids' => array())); //blank for all courses
		$result['courses'] = drudle_api_parsexml(drudle_api_callrest($method['courses']['method'], $method['courses']['arguments'], $domain_name, $security_key));

		$method['drudle']['method'] = 'local_drudle_hello_world';
		$method['drudle']['arguments'] = array('welcomemessage' => 'Hello Drupal, from '); //blank for all courses
		$result['hello'] = drudle_api_parsexml(drudle_api_callrest($method['drudle']['method'], $method['drudle']['arguments'], $domain_name, $security_key));
	}
	return '<pre>'.print_r($result, true).'</pre>';
}