<?php

/**
 * Call REST method in Moodle Web Service.
 *
 * @param $method
 *   The method function to be executed by the REST Service.
 * 
 * @param $arguments
 *   The variables to be passed to the method function.
 * 
 * @param $domain_name
 *	 The URL of Moodle
 * 
 * @param $security_key
 *	 Unique string which identifies user used for this REST call
 * 
 * @param $format
 *	 REST returned value format
 *	 Also possible in Moodle 2.2 and later: 'json'.
 *	 Setting it to 'json' will fail all calls on earlier Moodle version
 * 
 * @return
 *   The compatible associative array.
 */
function drudle_api_callrest($method, $arguments, $domain_name = NULL, $security_key = NULL, $format = 'xml') {
	$result = false;
	$url = $domain_name . '/webservice/rest/server.php'. '?wstoken=' . $security_key . '&wsfunction='.$method;

	module_load_include('php', 'drudle', 'includes/curl');
	$curl = new curl;

	//if rest format == 'xml', then we do not add the param for backward compatibility with Moodle < 2.2
	$format = ($format == 'json') ? '&moodlewsrestformat=' . $format : '';
	$result = $curl->post($url . $format, $arguments);
	
	return $result;
}

/**
 * Returns the associative array from Moodle REST XML output.
 *
 * @param $xml
 *   The XML result from Moodle REST Web Service.
 * 
 * @return
 *   The compatible associative array.
 */
function drudle_api_parsexml($xml) {
	$result = array();
	
	// Convert to XML Object
	$simplexml = simplexml_load_string($xml);
	$response = $simplexml->children();
	$multiple = $response->children();
	
	if(count($multiple)) {
		foreach($multiple as $single) {
			foreach($single->children() as $key) {
				$name = (string)$key->attributes()->name;
				$attr[$name] = (string)$key->children()->VALUE;
			}
			$result[] = $attr;
		}
	} else { // this means a single key response
		$result = (string)$response->VALUE;
	}
	
	return $result;
}